# -*- coding: utf-8 -*-
import re
from nltk.stem.cistem import Cistem


def tokenize_split(line):
    """
    generator for skr keywords, first yield is always the skr konto number
    :param line:
    :return:
    """
    # german word stemmer
    stemmer = Cistem()

    stopwords = ["april",
                 "aug.",
                 "august",
                 "dez.",
                 "dezember",
                 "feb.",
                 "febr.",
                 "februar",
                 "jan.",
                 "januar",
                 "jul.",
                 "juli",
                 "juli",
                 "jun.",
                 "juni",
                 "juni",
                 "mai",
                 "mai",
                 "mär.",
                 "märz",
                 "märz",
                 "mrz.",
                 "nov.",
                 "november",
                 "okt.",
                 "oktober",
                 "sep.",
                 "sept.",
                 "september"]
    # cast to lower and split line at all non german word characters
    nonwords = re.compile(r"[^\wÄÖÜäöüß]")
    nonnums = re.compile(r"^\D*$")
    words = [w for w in nonwords.split(line.lower()) if w not in stopwords]
    yield words[0]
    for w in words:
        if (len(w) >= 2) and nonnums.match(w):
            yield stemmer.stem(w)


if __name__ == '__main__':
    pass
