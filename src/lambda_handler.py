# -*- coding: utf-8 -*-
from lambda_find import finder, parse_userquery


def lambda_handler(event, context):
    skr = event['skr']
    query = event['query']

    res = finder(skr, parse_userquery(query))

    return {'kontos': res[0]}


def main():
    print(lambda_handler({'skr': 'skr05',
                          'query': "www"},
                         None))


if __name__ == '__main__':
    main()
