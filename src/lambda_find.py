# -*- coding: utf-8 -*-
import boto3
import tempfile
import os.path as path
from whoosh.qparser import QueryParser
from whoosh import index
from tokenize_clean import tokenize_split


def parse_userquery(query):
    lst = tokenize_split(query)
    return " OR ".join(lst)


def finder(subbucket, anfrage):
    """
    findet skr kontos passend zu worten in anfrage
    :param subbucket: kontenrahmen entspricht unterordner im bucket mit index
    für diesen rahmen
    :param anfrage: suchworte zu denen kontos gefunden werden sollen
    :return: tuple: liste mit konten und liste mit entsprechenden confidencen
    """
    #index von s3 bucket laden
    s3 = boto3.resource('s3')
    bucket = s3.Bucket("skr-search-index")

    with tempfile.TemporaryDirectory() as tempdir:
        tdir = tempdir
        for k in bucket.objects.all():
            p = path.split(k.key)
            # print(p)
            if subbucket in p:
                bucket.download_file(k.key, path.join(tdir, p[1]))

        try:
            idx = index.open_dir(tdir)
        except index.EmptyIndexError:
            return [], []

        # search for given wordes in now loaded index
        qp = QueryParser("content", schema=idx.schema)
        q = qp.parse(anfrage)
        with idx.searcher(closereader=True) as searcher:
            res = searcher.search(q)
            # print(list(res))
            out = [i["title"] for i in res]
            scores = [i.score for i in res]
        res = None
        searcher = None
        idx.close()
    return out, scores


def main():
    print(finder("skr04", parse_userquery("Telekom Rechnung")))


if __name__ == '__main__':
    main()
