# -*- coding: utf-8 -*-
import re
import os
import os.path as path
import sys
import boto3
from whoosh import index
from whoosh.fields import SchemaClass, TEXT
from tokenize_clean import tokenize_split


class MySchema(SchemaClass):
    """
    fill custom scema with columns for the search index
    see also whoosh doku
    """
    title = TEXT(stored=True)
    content = TEXT(stored=True)


def main(filename, skr):
    print(filename)
    with open(filename, 'rb') as f:
        content = f.read().decode("utf-8")
    linebreaks = r"\r\n?|\n"
    lbs = re.compile(linebreaks)

    # create database like scema in file system
    schema = MySchema()
    os.makedirs(skr, exist_ok=True)
    idx = index.create_in(skr, schema)
    writer = idx.writer()

    # add words for each konto to search index
    kontos = lbs.split(content)
    for kto in kontos:
        current = list(tokenize_split(kto))
        if current:
            writer.add_document(title=current[0],
                                content=current)
    writer.commit()

    # upload all files for the search index to s3 bucket folder for this
    # kontorahmen
    s3 = boto3.resource('s3')
    buck = "skr-search-index"
    bucket = s3.Bucket(buck)
    for root, dirs, files in os.walk(skr):
        for filename in files:
            p = path.join(root, filename)
            print("uploading {}...".format(p))
            bucket.upload_file(p, p.replace("\\", "/"))
            print("uploaded")

    return True


if __name__ == '__main__':
    try:
        main(sys.argv[1], sys.argv[2])
    except IndexError:
        print("please enter file path")
        exit(-1)
