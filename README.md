# SKR Stichwort Suche
Erstellung eines Suchindexes um mit Stichworten ein relevantes skr Konto zu
 finden. Weitere informationen siehe whoosh package
 https://whoosh.readthedocs.io/en/latest/intro.html
 
 ## Trainingsprozess
 Skript ``idx_create_n_up.py`` aufrufen mit ``stichworte.txt`` und
  Kontorahmen (skr03, skr04, ...) als übergabeparameter.
 
`` python idx_creat_n_up.py stichworte_skr04.txt skr04``

  Extrahiert stichworte für jedes Konto und fügt diese einem index hinzu.
    Index besteht aus Dateien im Dateisystem. Anschließend wird dieser in
     entsprechenden s3 Bucket hochgeladen wo er für die Lambdafunktion zur 
     verfügung steht.
  
  ### Spezifikationen ``stichworte.txt``
  Stichworte pro Skr Konto müssen vorher seperat bestimmt werden. In diesem
   fall hat das ein Labeler Hiwi vorgenommen.
  Diese Datei muss zeilenweise die skr Kontonummer gefolgt von allen
   für dieses Konto relevanten Stichworten Tab getrennt enthalten.
   
   Beispiel:
```
kotnonummer   stichworte1  stichworte2  ... 
5200	Wareneingang	5200 bez. Leistungen zu Leserreisen	Fertig- und Einbauteile
6800	Porto	 Deutsche Post Zahlungs	000011345639WIRECARD Online Services Post AG
```
(ohne header zeile)

 Mehrfachnennungen sind möglich und sogar förderlich um diesem Stichwort im
  Suchindex
  mehr Gewicht zu verleihen. 

 ## Vorhersage
 Lambdafunktion erhält Anfrage mit Kontenrahmen und Stichworten. Damit werd
  der entsprechende Index aus dem s3 Bucket geladen und in Tempfiles
   geschrieben.
   
   Die Stichworte werden dann im Index gesucht und, falls erfolgreich, wird
   ein array mit passenden Kontos zurück gegeben.
 
 